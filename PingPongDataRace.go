package main
import (
   "fmt"
   "strconv"
   "os"
   "sync"
   "time"
)
var wg sync.WaitGroup
func main(){
   num, _ := strconv.Atoi(os.Args[1])
   wg.Add(2)
   go ping(num)
   go pong(num)
   wg.Wait()
}

func ping(num int){
	for i := 0; i < num / 2; i++ {
		fmt.Fprintf(os.Stderr, "ping\n")
	}
	wg.Done()
}

func pong(num int){
	for i := 0; i < num / 2; i++ {
	  fmt.Fprintf(os.Stderr, "pong\n")
      time.Sleep(100 * time.Millisecond)
	}
	wg.Done()
}