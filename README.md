# Final Report

## How to run the files
To run the GoLang files, run them with command:


```Go
go run <filename.go> 100
```

To run the Elixir Infinite Stack Push file, run with command:


```Elixir
elixir PingPongInfiniteStackPush.ex
```