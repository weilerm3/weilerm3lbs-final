import :timer, only: [ sleep: 1 ]

defmodule PingPong do
 
   def new_ping() do
     pid = spawn_link(__MODULE__, :listen_ping, [])
     Process.register(pid, :Ping)
     pid
   end
   
   def new_pong() do
     pid = spawn_link(__MODULE__, :listen_pong, [])
     Process.register(pid, :Pong)
     pid
   end
   
   def listen_ping() do
		receive do
            {:print, {}} ->
				IO.puts("ping")
				send(:Pong, {:print, {}})
        end
		listen_ping()
   end
   
   def listen_pong() do
		receive do
            {:print, {}} ->
				IO.puts("pong")
				send(:Ping, {:print, {}})
        end
		listen_pong()
   end
   
   def wait_for_end() do
		receive do
			{:EXIT, _pid} ->
				IO.puts("Process Exited")
		end
   end
   
end

PingPong.new_ping()
PingPong.new_pong()
send(:Ping, {:print, {}})
PingPong.wait_for_end()