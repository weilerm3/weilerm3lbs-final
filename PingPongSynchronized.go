package main
import (
   "fmt"
   "strconv"
   "os"
   "sync"
   "time"
)
var wg sync.WaitGroup
var pingch chan int = make(chan int, 1)
var pongch chan int = make(chan int, 1)
func main(){
   num, _ := strconv.Atoi(os.Args[1])
   pingch <- 1
   wg.Add(2)
   go ping(num)
   go pong(num)
   wg.Wait()
}

func ping(num int){
	for i := 0; i < num / 2; i++ {
		<-pingch
		fmt.Fprintf(os.Stderr, "ping\n")
		time.Sleep(100 * time.Millisecond)
		pongch <- 1
	}
	wg.Done()
}

func pong(num int){
	for i := 0; i < num / 2; i++ {
		<- pongch
		fmt.Fprintf(os.Stderr, "pong\n")
		pingch <- 1
	}
	wg.Done()
}